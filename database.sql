-- Create money_manager database
CREATE DATABASE IF NOT EXISTS money_manager;

-- Use money_manager database
USE money_manager;

-- Create account table
CREATE TABLE IF NOT EXISTS account (
  id INT(10) PRIMARY KEY,
  name VARCHAR(255),
  user_id INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Create transaction table
CREATE TABLE IF NOT EXISTS transaction (
  id INT(10) PRIMARY KEY,
  name VARCHAR(255),
  account_id INT(10),
  user_id INT(10),
  amount DECIMAL(10, 2),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  FOREIGN KEY (account_id) REFERENCES account(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Create users table
CREATE TABLE IF NOT EXISTS users (
  id INT(10) PRIMARY KEY,
  email VARCHAR(100),
  password VARCHAR(100),
  created_at TIMESTAMP,
  updated_at TIMESTAM
)

