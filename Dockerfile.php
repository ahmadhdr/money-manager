FROM php:8.0-fpm

# Install dependencies
RUN apt-get update && apt-get install -y \
    curl \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath

# Set working directory
WORKDIR /var/www/html

# Copy application files
COPY ./src /var/www/html

# Set file permissions
RUN chown -R www-data:www-data /var/www/html \
    && chmod -R 755 /var/www/html

# Expose port (if necessary)
# EXPOSE 9000

# Start PHP-FPM server
CMD ["php-fpm"]