<?php
	require_once('./auth.php');
	session_start();
	if (isset($_SESSION['user']))
		header('Location: index.php');
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$auth = new Auth();
		$email = $_POST['email'];
		$password = $_POST['password'];
		$user = $auth->login($email, $password);
		if (!$user)
		{
			$_SESSION['error'] = 'Username atau password salah';
			header('Location: login.php');
			return;
		}
		$_SESSION['user'] = $user;
		header('Location: index.php');
		exit();
	}
	$has_error = FALSE;
	if (isset($_SESSION['error']))
		$has_error = TRUE;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login page | Money Manager</title>
	<link rel="stylesheet" href="./assets/base.css">
	<link rel="stylesheet" href="./assets/font.css">
	<link rel="stylesheet" href="./assets/style.css">
</head>
<body>
<div class="login">
	<div class="logo">
		<img src="https://saweria.co/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhomepage_characters.a1cf6cc4.svg&w=3840&q=75" alt="">
	</div>
	<h2>Money Manager</h2>
	<h3>Login</h3>
	<form method="POST" action="login.php">
		<div class="form-group <?php echo $has_error ? 'error' : '' ?>">
			<label for="email">Email: <span class="error">*</span></label>
			<input type="email" v-model="form.email" name="email" id="email" placeholder="email@pribadi.com">
			<!--<span>Email tidak boleh kosong</span>-->
		</div>
		<div class="form-group <?php echo $has_error ? 'error' : '' ?>">
			<label for="email">Password: <span class="error">*</span></label>
			<input v-model="form.password" type="password" name="password" id="password">
			<span <?php echo $has_error ? '' : 'hidden' ?>>
				<?php echo $_SESSION['error']; ?>
			</span>
		</div>
		<button>Login</button>
	</form>
  </div>
</body>
</html>