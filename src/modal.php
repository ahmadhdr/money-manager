<div id="modal-container">
	<div id="modal-content">
		<div class="modal-title">
			<h2>Tambah Dompet</h2>
			<span class="close pointer">&times;</span>
		</div>
		<div class="modal-body">
			<form id="form">
				<div class="form-group">
					<label>Nama Akun</label>
					<input type="text">
				</div>

				<div class="form-group">
					<label>Saldo Awal</label>
					<input type="number">
				</div>

				<div class="form-group">
					<label>Warna Latar</label>
					<input type="color">
				</div>
				<button class="btn-full">Simpan</button>
			</form>
		</div>
	</div>
</div>
