<?php



class Database {
	private $_host = 'mysql';
	private $_username = 'root';
	private $_database = 'money_manager';
	private $_password = 'password';

	private $_INSTANCE = NULL;
	public function getConnection()
	{
		if ($this->_INSTANCE != NULL)
			return $this->_INSTANCE;
		try {
			$conn = new PDO("mysql:host=$this->_host;dbname=$this->_database", $this->_username, $this->_password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->_INSTANCE = $conn;
			return $this->_INSTANCE;
		} catch(PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}	
	}
}
?>