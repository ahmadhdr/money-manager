<?php
require_once('./db.php');
class Auth { 
	private $db;
	public function __construct() 
	{
		$database = new Database();
		$this->db = $database->getConnection();
	}

	public function login($email, $password) 
	{
		 $query = $this->db->prepare("SELECT * FROM users WHERE email = :email");
		 $query->bindParam(':email', $email);
		 $query->execute();
		 $user = $query->fetch(PDO::FETCH_ASSOC);
		// Verify password
		if ($user && password_verify($password, $user['password'])) {
			return $user;
		} else {
			return FALSE;
		}
	}
}