<?php 
	session_start();
	if (!isset($_SESSION['user']))
		header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Homepage | Money Manager</title>
	<link rel="stylesheet" href="./assets/css/base.css">
	<link rel="stylesheet" href="./assets/css/font.css">
	<link rel="stylesheet" href="./assets/css/style.css">
	<link rel="stylesheet" href="./assets/css/modal.css">
</head>
<body>
	<?php include_once('modal.php'); ?>
	<div class="homepage">
		<div class="navbar">
			<div class="logo">
				<img src="https://saweria.co/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhomepage_characters.a1cf6cc4.svg&w=3840&q=75" alt=""> Atur Keuangan
			</div>
			<button class="btn pointer">
				<span>
					<?php echo substr($_SESSION['user']['email'], 0, 1); ?>
				</span>
				<?php echo $_SESSION['user']['email']; ?>
			</button>
		</div>
		
		<div class="main">
			<div class="header mt-20">
				<h1>Dompet Kamu</h1>
				<div class="summary">
					<p>Total Saldo Kamu</p>
					<h1><span>Rp</span> 75.0000.000</h1>
				</div>
			</div>
			

			<div class="w-account">
				<div class="box box-success">
					<h2>BCA</h2>
					<small>Saldo</small>
					<p><span>Rp</span> 10.000</p>
				</div>
				<div class="box box-default box-add pointer">
					<img src="./assets/plus.png" alt="">
					<h2>Buat Akun Baru</h2>
				</div>
			</div>

			<div class="header mt-20">
				<h1>Transaksi</h1>
				<input type="date">
			</div>

			<div class="w-transaction">
				<div class="item">
					<div class="title">
						<h2>Belanja bulananan</h2>
						<h1 id="account-name">
							BCA
						</h1>
					</div>
					<div class="description">
						<h2 class="text-danger">-Rp <span id="nominal">10.000</span></h2>
						07.22
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>